# README #

Cycles automatic materials is an addon for [Blender 3D](http://www.blender.org). Its purpose is to simplify the setup of a material: almost all materials have a common structure:

- diffuse
- glossy
- mix
- normal or bumpmap
- specularity map
- (optional) occlusion map

This addon creates a material from an image texture. It sets up a diffuse, glossy and mix shader. The material's shader nodes also generate a bump and specularity map internally in real time. It can either create a fully automatic material, or let the user specify the values (global glossyness, global bump strength), that are very hard to guess right programatically. The automatic option also tries to detect pre-generated bump, specular and normal maps.

Cycles automatic material v. 0.39

### New in 0.39 ###

* Added actual UI
* Added Make Seamless

## Download


![folder-download.png](https://bitbucket.org/repo/r5bG8X/images/2677033194-folder-download.png)

[[Download here]](https://bitbucket.org/Theo_Friberg/cycles-automatic-materials-addon/downloads/automat.zip)

## Setup

To install Cycles automatic materials, just download the AutoMat.py. Then, in Blender, go to: User preferences > Addons > Install from file > select AutoMat.py > enable the addon. Now you are ready to use the addon.

## Using Cycles automatic materials

Here are step by step instructions for using the addon:

Make sure that

- Cycles is the active renderer

![cycles as active.png](https://bitbucket.org/repo/r5bG8X/images/1241842706-cycles%20as%20active.png)

- The addon is enabled (see _Setup_ above)

![addon active.png](https://bitbucket.org/repo/r5bG8X/images/2476363426-addon%20active.png)

- Your object is UV-unwrapped. You can do this later too.

Select the object you want to add the material to.

Press space and search for "Automatic material from texture".

![search operator.png](https://bitbucket.org/repo/r5bG8X/images/1172660956-search%20operator.png)

The feature can also be accessed via File > Import > Automatic Material from Image.

![Menu.png](https://bitbucket.org/repo/r5bG8X/images/3271880544-Menu.png)


A file browser appears. Select your texture. Note, that there is an option to make the image seamless (this doesn't modify the original image. It creates an effect comparable to (and inspired by) GIMP's [Make Seamless -filter](http://docs.gimp.org/en_GB/plug-in-make-seamless.html) solely inside the Node Editor.):

![Make Seamless.png](https://bitbucket.org/repo/r5bG8X/images/3250952238-Make%20Seamless.png)

The material is now added to your object. If it doesn't show or it looks weird:

 - No visible change: The material may not be assigned.
 - The object is of one solid color that is not the "missing texture" pink: The object is not UV-unwrapped.
 - The object is "error pink": Is your texture right? If you are sure it is not a problem with the texture, did someone send you the .blend file? If so, do you miss files? If neither of these apply, please tell me at [theo.friberg@gmail.com](mailto:theo.friberg@gmail.com). I would be glad if you sent me the .blend and, if possible the texture (a .zip containing both would be best) along with information of how the error occured.

There is also a more tweakable version of the material. However, as the original version has undergone massive refactoring, the "Adjustable Material from Image" is now deprecated. Here is nonetheless how to use it:

![setup.png](https://bitbucket.org/repo/r5bG8X/images/607853486-setup.png)

## Bugs & critique

As the thing is still in a very early state of development, any critique or tests are welcome. Please send me an email at [theo.friberg@gmail.com](mailto:theo.friberg@gmail.com). Also, if you experience errors, email me with the faulty .blend and texture if available.

## Acknowledgements

The blur I use is based on work by Sebastian “bashi” Röthlisberger, see here:
https://bwide.wordpress.com/node-groups/bwide-nodepack-for-blender/
for the rest of his nodes.  The blur is called ImageBlur.